CFLAGS ?= -std=c99 -pedantic -Wall -Wextra -O2

CFLAGS += -pedantic-errors

# GCC >= 4.6
CFLAGS += -Wunused-but-set-variable

CFLAGS += -fno-common \
  -Wall \
  -Wextra \
  -Wformat=2 \
  -Winit-self \
  -Winline \
  -Wpacked \
  -Wp,-D_FORTIFY_SOURCE=2 \
  -Wpointer-arith \
  -Wlarger-than-65500 \
  -Wmissing-declarations \
  -Wmissing-format-attribute \
  -Wmissing-noreturn \
  -Wmissing-prototypes \
  -Wnested-externs \
  -Wold-style-definition \
  -Wredundant-decls \
  -Wsign-compare \
  -Wstrict-aliasing=2 \
  -Wstrict-prototypes \
  -Wswitch-enum \
  -Wundef \
  -Wunreachable-code \
  -Wunsafe-loop-optimizations \
  -Wwrite-strings

# for some functions in endian.h
CFLAGS += -D_BSD_SOURCE

# for getopt()
CFLAGS += -D_POSIX_C_SOURCE=2

PREFIX ?= /usr/local
bindir := $(PREFIX)/sbin

actions-firmware-extract: actions-firmware-extract.o

test: actions-firmware-extract
	rm -rf PPX2230 && \
	./actions-firmware-extract -d PPX2230 ppx2230_eu_fus_aen.bin

install: actions-firmware-extract
	install -d $(DESTDIR)$(bindir)
	install -m 755 actions-firmware-extract $(DESTDIR)$(bindir)

clean:
	rm -rf *~ *.o actions-firmware-extract endian endian.h
